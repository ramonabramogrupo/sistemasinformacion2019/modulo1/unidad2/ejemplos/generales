﻿USE paginaweb;

/*
  1. Titulo del libro y nombre del autor que lo ha escrito
*/

  -- Con Join
  SELECT l.titulo, a.nombre_completo 
    FROM libro l JOIN autor a 
    ON l.autor = a.id_autor;

  -- Sin Join (producto cartesiano)
  SELECT l.titulo, a.nombre_completo
    FROM libro l, autor a
    WHERE l.autor = a.id_autor;

  -- Con Join como producto cartesiano (no utilizar)
  SELECT l.titulo, a.nombre_completo 
    FROM libro l JOIN autor a 
    WHERE l.autor = a.id_autor;

/*
  2. Nombre del autor y titulo del libro en el que han ayudado
*/

  SELECT a1.nombre_completo, l.titulo 
    FROM libro l 
    JOIN ayuda a ON l.id_libro = a.libro 
    JOIN autor a1 ON a.autor = a1.id_autor;

/*
  3. Los libros que se ha descargado cada usuario con la fecha de descarga. 
  Listar el login, fecha de descarga, id_libro
*/
  
  SELECT f.libro,
         f.usuario,
         f.fecha_descarga
    FROM fechadescarga f;

/*
  4. Los libros que se ha descargado cada usuario con la fecha de descarga. 
  Listar el login, correo,  fecha de descarga, titulo del libro
*/

  SELECT u.login, u.email, f.fecha_descarga, l.titulo
    FROM usuario u 
    JOIN descarga d ON u.login = d.usuario
    JOIN fechadescarga f USING (usuario, libro)
    JOIN libro l ON d.libro = l.id_libro;

  SELECT u.login, u.email, f.fecha_descarga, l.titulo
    FROM usuario u 
    JOIN descarga d 
    JOIN fechadescarga f 
    JOIN libro l
    ON d.libro = l.id_libro 
    AND u.login = d.usuario 
    AND d.usuario = f.usuario 
    AND d.libro = f.libro;