﻿/*
  1- Titulo del libro y nombre del autor que lo ha escrito
*/

-- producto cartesiano
  SELECT a.nombre_completo,l.titulo 
    FROM libro l,autor a
    WHERE l.autor = a.id_autor;
    
-- join 
  SELECT a.nombre_completo,l.titulo
    FROM libro l JOIN autor a 
    ON l.autor = a.id_autor;

-- join como producto cartesiano (no utilizar)
  SELECT a.nombre_completo,l.titulo
    FROM libro l JOIN autor a 
    WHERE l.autor = a.id_autor;

/*
  2- nombre del autor y titulo del libro en 
  el que han ayudado
*/

  SELECT au.nombre_completo, l.titulo
    FROM libro l 
    JOIN ayuda ay ON l.id_libro = ay.libro
    JOIN autor au ON ay.autor = au.id_autor; 

  SELECT nombre_completo,titulo 
    FROM libro JOIN autor JOIN ayuda
    ON autor.id_autor = ayuda.autor 
    AND libro.id_libro = ayuda.libro;

  /*
    3- los libros que se ha descargado cada usuario con la fecha de descarga. Listar el login, fecha descarga, id_libro
  */

SELECT f.libro,
       f.usuario,
       f.fecha_descarga 
    FROM fechadescarga f;

/*
    4- los libros que se ha descargado cada usuario con la fecha de descarga. Listar el login,correo, fecha descarga, titulo del libro
*/

    SELECT u.login, u.email, f.fecha_descarga, l.titulo 
      FROM fechadescarga f JOIN descarga d
      ON f.libro = d.libro AND f.usuario = d.usuario
      JOIN usuario u ON d.usuario = u.login
      JOIN libro l ON d.libro = l.id_libro;

SELECT * 
  FROM fechadescarga f JOIN descarga d  
  JOIN usuario u JOIN libro l
  ON f.libro = d.libro AND f.usuario = d.usuario AND d.usuario = u.login AND d.libro = l.id_libro; 

/*
  5- El numero de libros que hay 
*/ 

  SELECT COUNT(*) nlibros FROM libro l;
     
  
/*
  6- El numero de libros por coleccion (no hace falta colocar nombre de la coleccion)
*/

  SELECT l.coleccion,COUNT(*) nlibros 
  FROM libro l 
  GROUP BY l.coleccion;

/*
  7- La coleccion que tiene mas libros (no hace falta colocar nombre de la coleccion)
*/

-- subconsulta C1

  SELECT l.coleccion,COUNT(*) nlibros 
  FROM libro l 
  GROUP BY l.coleccion;

-- subconsulta C2

  SELECT MAX(c1.nlibros) maximo FROM 
    (
      SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
    ) c1;

-- final
  SELECT c1.coleccion 
    FROM (
      SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
    ) c1 
    JOIN (
          SELECT MAX(c1.nlibros) maximo FROM 
            (
              SELECT l.coleccion,COUNT(*) nlibros 
              FROM libro l 
              GROUP BY l.coleccion
            ) c1
    ) c2
    ON c1.nlibros=c2.maximo;

  -- final con where
  SELECT * FROM 
    (
      SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
    ) c1
    WHERE nlibros=
      (
        SELECT MAX(c1.nlibros) maximo FROM 
        (
          SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      );

  -- final con having
    SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
      HAVING nlibros=
      (
        SELECT MAX(c1.nlibros) maximo FROM 
        (
          SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      );
  
/* 
  8- La coleccion que tiene menos libros (no hace falta colocar nombre de la coleccion)
*/

  -- subconsulta C1

  SELECT l.coleccion,COUNT(*) nlibros 
  FROM libro l 
  GROUP BY l.coleccion;

-- subconsulta C2

  SELECT MIN(c1.nlibros) minimo FROM 
    (
      SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
    ) c1;

  -- final 
    SELECT c1.coleccion FROM 
      (
        SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1 
      JOIN 
      (
        SELECT MIN(c1.nlibros) minimo FROM 
          (
            SELECT l.coleccion,COUNT(*) nlibros 
            FROM libro l 
            GROUP BY l.coleccion
          ) c1
      ) c2
      ON c1.nlibros=c2.minimo;
-- final con where

SELECT * FROM 
  (
    SELECT l.coleccion,COUNT(*) nlibros 
    FROM libro l 
    GROUP BY l.coleccion
  ) c1
  WHERE 
    c1.nlibros=(
      SELECT MIN(c1.nlibros) minimo FROM 
      (
        SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    );

-- final having

  SELECT l.coleccion,COUNT(*) nlibros   
    FROM libro l 
    GROUP BY l.coleccion
    HAVING 
      nlibros=(
        SELECT MIN(c1.nlibros) minimo FROM 
        (
          SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      );

/*
  9- El nombre de la coleccion que tiene mas libros
*/
  SELECT c.nombre FROM coleccion c 
    JOIN (
            SELECT c1.coleccion 
          FROM (
            SELECT l.coleccion,COUNT(*) nlibros 
            FROM libro l 
            GROUP BY l.coleccion
          ) c1 
          JOIN (
                SELECT MAX(c1.nlibros) maximo FROM 
                  (
                    SELECT l.coleccion,COUNT(*) nlibros 
                    FROM libro l 
                    GROUP BY l.coleccion
                  ) c1
          ) c2
          ON c1.nlibros=c2.maximo
    ) consulta7
    ON c.id_coleccion=consulta7.coleccion;


/* 
  10- El nombre de la coleccion que tiene menos libros
*/

  SELECT c.nombre FROM coleccion c 
    JOIN (
    SELECT c1.coleccion FROM 
      (
        SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1 
      JOIN 
      (
          SELECT MIN(c1.nlibros) minimo FROM 
            (
              SELECT l.coleccion,COUNT(*) nlibros 
              FROM libro l 
              GROUP BY l.coleccion
            ) c1
        ) c2
        ON c1.nlibros=c2.minimo
    ) consulta8
    ON c.id_coleccion=consulta8.coleccion;

/* 
  11- El nombre del libro que se ha descargado mas veces
*/

  -- c1
  -- el numero de descargas por cada libro

  SELECT f.libro,COUNT(*) ndescargas 
    FROM fechadescarga f
    GROUP BY f.libro;

  -- c2 
  -- el numero maximo de descargas por libro
  SELECT MAX(c1.ndescargas) maximo FROM (
      SELECT f.libro,COUNT(*) ndescargas 
      FROM fechadescarga f
      GROUP BY f.libro
    ) c1;

  -- c3
  -- codigo del libro que se ha descargado mas veces

    SELECT c1.libro FROM (
        SELECT f.libro,COUNT(*) ndescargas 
        FROM fechadescarga f
        GROUP BY f.libro
      ) c1
      JOIN (
            SELECT MAX(c1.ndescargas) maximo FROM (
                  SELECT f.libro,COUNT(*) ndescargas 
                  FROM fechadescarga f
                  GROUP BY f.libro
                ) c1        
      ) c2
      ON c1.ndescargas=c2.maximo;

    -- final
      SELECT l.titulo FROM libro l
        JOIN (
               SELECT c1.libro FROM (
                      SELECT f.libro,COUNT(*) ndescargas 
                      FROM fechadescarga f
                      GROUP BY f.libro
                    ) c1
                    JOIN (
                          SELECT MAX(c1.ndescargas) maximo FROM (
                                SELECT f.libro,COUNT(*) ndescargas 
                                FROM fechadescarga f
                                GROUP BY f.libro
                              ) c1        
                    ) c2
                    ON c1.ndescargas=c2.maximo
        ) c3
        ON c3.libro=l.id_libro;

/* 
  12- El nombre del usuario que ha descargado mas 
  libros
*/

  -- c1
  -- numero de descargas por usuario

SELECT f.usuario,COUNT(*) ndescargas 
  FROM fechadescarga f GROUP BY f.usuario;

-- c2
-- el numero maximo de descargas por usuario
  SELECT MAX(c1.ndescargas) maximo FROM 
    (
        SELECT f.usuario,COUNT(*) ndescargas 
          FROM fechadescarga f GROUP BY f.usuario
    ) c1;

-- final con join (opcion 1)
  SELECT c1.usuario FROM 
    (
      SELECT f.usuario,COUNT(*) ndescargas 
        FROM fechadescarga f GROUP BY f.usuario
    ) c1
    JOIN
    (
      SELECT MAX(c1.ndescargas) maximo FROM 
          (
              SELECT f.usuario,COUNT(*) ndescargas 
                FROM fechadescarga f GROUP BY f.usuario
          ) c1
    ) c2
    ON c1.ndescargas=c2.maximo;

-- final con having (opcion 2)

SELECT f.usuario,COUNT(*) ndescargas 
  FROM fechadescarga f GROUP BY f.usuario
  HAVING ndescargas=
  (
    SELECT MAX(c1.ndescargas) maximo FROM 
        (
            SELECT f.usuario,COUNT(*) ndescargas 
              FROM fechadescarga f GROUP BY f.usuario
        ) c1
  );

/*
  13- El nombre de los usuarios que han descargado mas libros que Adam3
*/

-- C1: numero de descargas de Adam3
SELECT COUNT(*) ndescargas 
  FROM fechadescarga f 
  WHERE f.usuario='Adam3';

-- C2: descargas que ha realizado cada usuario
SELECT f.usuario,COUNT(*) nlibros 
  FROM fechadescarga f
  GROUP BY usuario;

-- consulta final
SELECT f.usuario,COUNT(*) nlibros 
  FROM fechadescarga f
  GROUP BY usuario
  HAVING nlibros>(
    SELECT COUNT(*) ndescargas 
    FROM fechadescarga f 
    WHERE f.usuario='Adam3'
  );


/* 
  14- El mes que mas libros se han descargado
*/


-- c1: el numero de descargas por mes
  SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga);

-- c2: el numero maximo de descargas por mes
  SELECT MAX(c1.ndescargas) maximo FROM 
    (
        SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
    ) c1;

-- final con join
  SELECT c1.mes FROM 
    (
      SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas 
      FROM fechadescarga f 
      GROUP BY MONTH(f.fecha_descarga)
    ) c1
    JOIN 
    (
      SELECT MAX(c1.ndescargas) maximo FROM 
        (
            SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas 
            FROM fechadescarga f 
            GROUP BY MONTH(f.fecha_descarga)
        ) c1
    ) c2
    ON c1.ndescargas=c2.maximo;

-- final con having

    SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga)
    HAVING ndescargas=   
    (
      SELECT MAX(c1.ndescargas) maximo FROM 
      (
          SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas 
          FROM fechadescarga f 
          GROUP BY MONTH(f.fecha_descarga)
      ) c1
    );