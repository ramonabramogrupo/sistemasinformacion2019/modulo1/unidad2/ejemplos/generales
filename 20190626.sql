﻿USE ciclistas;

-- numero de etapas que hay

  SELECT COUNT(*) total FROM etapa e;

  SELECT COUNT(e.dorsal) total FROM etapa e;

-- numero de maillots que hay
SELECT COUNT(*) total FROM maillot m;

-- numero de ciclistas que hayan ganado alguna etapa

-- sin subconsultas
SELECT COUNT(DISTINCT e.dorsal) FROM etapa e;

-- con subconsultas
  -- c1: dorsal de los ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- final
  SELECT COUNT(*) 
    FROM (SELECT DISTINCT e.dorsal FROM etapa e) c1;
