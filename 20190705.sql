﻿/*
  cuantas etapas ha ganado cada ciclista  

  dorsal,numeroEtapas
  1,3
  2,1
*/

  SELECT e.dorsal,COUNT(*) numeroEtapas
  FROM etapa e 
  GROUP BY e.dorsal;

/*
  dorsal de los ciclistas han ganado 
  mas de 2 etapas
*/

    SELECT e.dorsal 
      FROM etapa e 
      GROUP BY e.dorsal
      HAVING COUNT(*)>2;

    SELECT dorsal FROM (
        SELECT e.dorsal,COUNT(*) numeroEtapas 
          FROM etapa e 
          GROUP BY e.dorsal
          HAVING numeroEtapas>2
        ) c1;


/*
  nombre de los ciclistas han ganado 
  mas de 2 etapas
*/

     SELECT c.nombre FROM (
        SELECT e.dorsal 
          FROM etapa e 
          GROUP BY e.dorsal
          HAVING COUNT(*)>2
      ) c1 JOIN ciclista c USING(dorsal);

    /*
      el nombre del ciclista que tiene mas
      edad
    */

      SELECT * FROM ciclista c 
        ORDER BY edad DESC LIMIT 1;

      -- c1 : edad maxima
        SELECT MAX(edad) maxima FROM ciclista c; 

       -- solucion1 : join
        SELECT c.nombre FROM ciclista c 
          JOIN (SELECT MAX(edad) maxima FROM ciclista c) c1
          ON c1.maxima=c.edad;
      




