﻿USE ciclistas;

/**
  proyeccion
**/

  SELECT DISTINCT c.nombre FROM ciclista AS c;
  
  SELECT DISTINCT c.nombre FROM ciclista c;

  SELECT DISTINCT ciclista.nombre FROM ciclista;
  
  SELECT DISTINCT nombre FROM ciclista;

/**
  seleccion
**/

  SELECT 
    * 
  FROM 
    ciclista c 
  WHERE 
    c.edad<30;

  SELECT * FROM ciclista c WHERE c.edad<30;


/*
  combino ambos operadores

*/

  SELECT DISTINCT 
    c.nombre 
  FROM 
    ciclista c 
  WHERE 
    c.edad<30;

-- indicame las edades de los ciclistas de banesto

  SELECT 
    DISTINCT c.edad
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto';
 

-- indicame los nombres de los equipos de los ciclistas 
-- que tienen menos de 30 años
    
  

SELECT 
  DISTINCT c.nomequipo 
FROM 
  ciclista c 
WHERE 
  c.edad<30;


/**
  and y or
**/

-- listar los nombres de los ciclistas
-- cuya edad esta entre 30 Y 32 (inclusive)

  SELECT 
    DISTINCT c.nombre 
  FROM 
    ciclista c 
  WHERE 
    c.edad>=30 AND c.edad<=32;

  -- modificar con los operadores extendidos
SELECT 
  DISTINCT c.nombre 
  FROM 
    ciclista c
  WHERE
    c.edad BETWEEN 30 AND 32;
    
-- listar los equipos que tengan ciclistas 
-- menores de 31 años y que su nombre comience por M

SELECT 
  DISTINCT c.nomequipo
FROM 
  ciclista c
WHERE 
    c.edad<31 AND c.nombre LIKE 'M%';


-- listar los equipos que tengan ciclistas 
-- menores de 31 años o que su nombre comience por M

SELECT 
  DISTINCT c.nomequipo
  FROM 
    ciclista c
  WHERE 
    c.edad<31 OR c.nombre LIKE 'M%';

-- listarme los ciclistas de banesto y de kelme

  SELECT * 
    FROM ciclista c
    WHERE c.nomequipo='banesto' OR c.nomequipo='kelme';

  -- operadores extendidos
SELECT 
  * 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo IN('banesto','kelme');









  
 