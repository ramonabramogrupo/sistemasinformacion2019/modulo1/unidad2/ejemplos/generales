﻿USE teoria1;
/*
  1- Numero de coches que ha alquilado el usuario 1
*/

  -- numero de alquilere
  SELECT COUNT(*) nalquileres 
    FROM alquileres a WHERE a.usuario=1;

  -- numero de coches
  SELECT COUNT(DISTINCT a.coche) ncoches 
    FROM alquileres a WHERE a.usuario=1;

  /*
    2- Numero de alquileres por mes
  */

    SELECT MONTH(fecha) mes,COUNT(*) numero 
      FROM alquileres a 
      GROUP BY MONTH(a.fecha);

/*
  3- Numero de usuarios por Sexo
*/

  SELECT u.sexo,COUNT(*) numero 
    FROM usuarios u 
    GROUP BY u.sexo;

/*
  4- Numero de alquileres de coches por color
*/

  SELECT c.color,COUNT(*) numero 
    FROM coches c
    JOIN alquileres a 
    ON c.codigoCoche = a.coche 
    GROUP BY c.color;

  /*
    5- Numero de marcas 
  */
  
  SELECT COUNT(DISTINCT c.marca) numero 
    FROM coches c;

  /*
    6- Numero de marcas de coches alquilados
  */

    SELECT c.marca,COUNT(*) numero 
      FROM coches c 
      JOIN alquileres a 
      ON c.codigoCoche = a.coche
      GROUP BY c.marca;

    -- otra interpretacion
    SELECT COUNT(DISTINCT c.marca) numero 
      FROM coches c
      JOIN alquileres a 
      ON c.codigoCoche = a.coche

  /*
    7- Poblacion con mas hombres
  */

  /*
    8- Poblacion con mas alquileres
  */

    

