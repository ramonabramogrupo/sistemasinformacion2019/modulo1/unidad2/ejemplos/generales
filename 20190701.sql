﻿USE ciclistas;

/* 
  ciclistas que han ganado etapas
  Mostrar dorsal
*/

  SELECT DISTINCT e.dorsal FROM etapa e;

/* 
  ciclistas que han ganado puertos
  Mostrar dorsal
*/

  SELECT DISTINCT p.dorsal FROM puerto p ;

  /*
    Dorsal de los ciclistas que han ganado
    etapas o puertos
  */

  SELECT DISTINCT e.dorsal FROM etapa e
    UNION  
  SELECT DISTINCT p.dorsal FROM puerto p;

  /*
    Dorsal de los ciclistas que han ganado
    etapas y puertos
  */
  
  /* SELECT DISTINCT e.dorsal FROM etapa e
    intersect 
  SELECT DISTINCT p.dorsal FROM puerto p;*/

    SELECT c1.dorsalEtapa dorsal FROM 
      (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
      JOIN 
      (SELECT DISTINCT p.dorsal dorsalPuerto FROM puerto p) c2
      ON c1.dorsalEtapa=c2.dorsalPuerto;


/*
    Listado de todas las etapas y 
    el ciclista que las ha ganado
*/

  SELECT * 
    FROM ciclista c JOIN etapa e 
    ON c.dorsal = e.dorsal;

/*
    Listado de todas los puertos y 
    el ciclista que las ha ganado
*/

  SELECT * 
    FROM puerto p JOIN ciclista c 
    ON p.dorsal = c.dorsal;



  /*
    combinaciones interna  
  */

  -- Listar todos los ciclistas con todos los datos del equipo 
  -- al que pertenecen 

  SELECT * 
    FROM equipo e JOIN ciclista c 
    ON e.nomequipo = c.nomequipo;

  SELECT * 
    FROM equipo e JOIN ciclista c
    USING(nomequipo);
  
  SELECT * 
    FROM equipo e JOIN ciclista c 
    WHERE e.nomequipo = c.nomequipo;
   
/*
  producto cartesiano
*/

  SELECT * 
    FROM ciclista c, equipo e;


-- convierto el producto cartesiano en una combinacion interna
    
SELECT * 
    FROM ciclista c, equipo e
    WHERE c.nomequipo=e.nomequipo;


/*
  Listarme los nombres del ciclista y del equipo de aquellos 
  ciclistas que hayan ganado puertos
*/

SELECT DISTINCT c.nombre,c.nomequipo 
  FROM ciclista c  JOIN puerto p ON p.dorsal = c.dorsal;

SELECT c.nombre,c.nomequipo 
  FROM ciclista c;




/*
  Listarme los nombres del ciclista y del equipo de aquellos 
  ciclistas que hayan ganado etapas
*/

  SELECT DISTINCT c.nombre,c.nomequipo
    FROM etapa e JOIN ciclista c 
    ON e.dorsal = c.dorsal;


-- c1 (subconsulta)
  SELECT DISTINCT e.dorsal 
  FROM etapa e; 

-- completa
  SELECT c.nombre,c.nomequipo 
    FROM ciclista c 
      JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1
      USING(dorsal);


  /**
    quiero saber los ciclistas que han ganado puertos y el numero 
    de puertos que han ganado. Del ciclista quiere saber el dorsal 
    y el nombre  
    dorsal,nombre,numeroPuertos
  */


    -- c1
    SELECT dorsal,c.nombre,p.nompuerto 
    FROM puerto p JOIN ciclista c USING(dorsal);

    -- completa
    SELECT c1.dorsal,c1.nombre,COUNT(*) numeroPuertos 
      FROM (
        SELECT c.dorsal,c.nombre,p.nompuerto 
        FROM puerto p JOIN ciclista c USING(dorsal)
      ) c1
      GROUP BY c1.dorsal,c1.nombre;
