﻿/**
  Consultas de la bbdd de pesca
**/

  USE pesca;

/* 
  1- Indica el nombre del club que tiene pescadores 
*/

  SELECT DISTINCT c.nombre 
    FROM clubes c JOIN pescadores p 
    ON c.cif = p.club_cif;




/* 
  2- indica el nombre del club que no tiene pescadores 
*/

  SELECT c.nombre
    FROM clubes c 
    LEFT JOIN pescadores p ON c.cif = p.club_cif
    WHERE p.club_cif is NULL;

/*
  3- indica el nombre de los cotos autorizados a algun club
*/

SELECT DISTINCT a.coto_nombre 
  FROM autorizados a; 

/*
  4- Indica la provincia que tiene cotos autorizados a algun club
*/

  SELECT DISTINCT provincia FROM cotos c 
    JOIN autorizados a ON c.nombre = a.coto_nombre;

/* 
  5-Indica el nombre de los cotos que no estan autorizados a ningun club 
*/

  SELECT c.nombre 
      FROM cotos c 
      LEFT JOIN autorizados a ON c.nombre = a.coto_nombre          WHERE a.coto_nombre IS NULL;
/*
  6- Indica el numero de rios por provincia con cotos
*/

  SELECT c.provincia,COUNT(DISTINCT c.rio) 
    FROM cotos c GROUP BY c.provincia;

/*
  7- Indica el numero de rios por provincia con cotos autorizados
*/
  SELECT provincia,COUNT(DISTINCT c.rio) numero 
    FROM cotos c JOIN autorizados a 
    ON c.nombre = a.coto_nombre
    GROUP BY c.provincia;

/*
  8- Indica el nombre de la provincia con mas cotos autorizados
*/

  -- c1: numero de cotos autorizados por provincia
  SELECT c.provincia,COUNT(DISTINCT c.nombre) numero 
    FROM cotos c 
    JOIN autorizados a ON c.nombre = a.coto_nombre
    GROUP BY provincia;

  -- c2 : numero maximo de cotos por provincia
    SELECT MAX(C1.numero) maximo 
      FROM 
      (
        SELECT c.provincia,COUNT(DISTINCT c.nombre) numero 
          FROM cotos c 
          JOIN autorizados a ON c.nombre = a.coto_nombre
          GROUP BY provincia
      )C1;

    -- final
      SELECT C1.provincia FROM 
        (
          SELECT c.provincia,COUNT(DISTINCT c.nombre) numero 
              FROM cotos c 
              JOIN autorizados a ON c.nombre = a.coto_nombre
              GROUP BY provincia
        ) C1
        JOIN 
        (
          SELECT MAX(C1.numero) maximo 
          FROM 
          (
            SELECT c.provincia,COUNT(DISTINCT c.nombre) numero 
              FROM cotos c 
              JOIN autorizados a ON c.nombre = a.coto_nombre
              GROUP BY provincia
          )C1
        ) c2
        ON C1.numero=c2.maximo; 

/*
  9- Indica el nombre del pescador y el nombre de su ahijado 
*/

  SELECT padrinos.nombre,ahijados.nombre
    FROM pescadores padrinos 
    JOIN apadrinar a ON padrinos.numSocio = a.padrino
    JOIN pescadores ahijados ON ahijados.numSocio=a.ahijado;

/*
  10- Indica el numero de ahijados de cada pescador
*/

  SELECT a.padrino,COUNT(*) numero 
    FROM apadrinar a 
    GROUP BY a.padrino;   



